﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectFade : MonoBehaviour {

	public SpriteRenderer sprite;

	public Color spriteColorFadeOut = Color.white;
	public Color spriteColorFadeIn = Color.white;
	public float fadeInTime = 0.5f;
	public float fadeOutTime = 0.2f;
	public float delayToFadeOut = 5f;
	public float delayToFadeIn = 5f;

	private bool isFadeIn = false;
	private bool isFadeOut = false;

	void Awake()
	{
		sprite = this.GetComponent<SpriteRenderer> ();

		//StartCoroutine("FadeOutCycle");
	}

	public void StartFadeOut()
	{
		isFadeOut = true;
		StartCoroutine ("FadeOutCycle");
	}

	public void StartFadeIn()
	{
		isFadeIn = true;
		StartCoroutine ("FadeInCycle");
	}

	IEnumerator FadeOutCycle()
	{
		float fade = 0.1f;
		float startTime;

		while (isFadeOut) {
			
			startTime = Time.time;

			while (fade > 0f) {

				fade = Mathf.Lerp (1f, 0f, (Time.time - startTime) / fadeOutTime);
				spriteColorFadeOut.a = fade;
				sprite.color = spriteColorFadeOut;
				yield return null;
			}

			fade = 0f;
			spriteColorFadeOut.a = fade;
			sprite.color = spriteColorFadeOut;
			//yield return new WaitForSeconds(delayToFadeIn);

			isFadeOut = false;
			//sprite.enabled = false;
		}
	}

	IEnumerator FadeInCycle()
	{
		float fade = 0.1f;
		float startTime;

		while (isFadeIn) {
			
			startTime = Time.time;

			while (fade < 1f) {
				
				fade = Mathf.Lerp (0f, 1f, (Time.time - startTime) / fadeInTime);
				spriteColorFadeIn.a = fade;
				sprite.color = spriteColorFadeIn;
				yield return null;
			}
				
			fade = 1f;
			spriteColorFadeIn.a = fade;
			sprite.color = spriteColorFadeIn;
			//yield return new WaitForSeconds(delayToFadeOut);

			isFadeIn = false;
		}
	}
}
