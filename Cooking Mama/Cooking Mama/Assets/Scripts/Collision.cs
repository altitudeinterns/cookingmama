﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collision : MonoBehaviour {

	private bool collEgg = false;

	private bool collWhisk = false;
	private bool collBowlLimitLeft = false;
	private bool collBowlLimitRight = false;

	private bool collSpatula = false;
	private bool collStoveLimitLeft = false;
	private bool collStoveLimitRight = false;

	void OnTriggerEnter2D(Collider2D other)
	{
		if (this.gameObject.name == "Bowl") {
			if (other.gameObject.name == "Egg" || other.gameObject.tag == "Egg") {
				collEgg = true;
			}
		} else if (this.gameObject.name == "BowlTriggerLeft") {
			if (other.gameObject.name == "Whisk") {
				collWhisk = true;
			}
		} else if (this.gameObject.name == "BowlTriggerRight") {
			if (other.gameObject.name == "Whisk") {
				collWhisk = true;
			}
		} else if (this.gameObject.name == "BowlLimitLeft") {
			if (other.gameObject.name == "Whisk") {
				collBowlLimitLeft = true;
				Debug.Log ("LEFT LIMIT HIT");
			}
		} else if (this.gameObject.name == "BowlLimitRight") {
			if (other.gameObject.name == "Whisk") {
				collBowlLimitRight = true;
				Debug.Log ("RIGHT LIMIT HIT");
			}
		} else if (this.gameObject.name == "StoveTriggerLeft") {
			if (other.gameObject.name == "Spatula") {
				collSpatula = true;
			}
		} else if (this.gameObject.name == "StoveTriggerRight") {
			if (other.gameObject.name == "Spatula") {
				collSpatula = true;
			}
		} else if (this.gameObject.name == "StoveLimitLeft") {
			if (other.gameObject.name == "Spatula") {
				collStoveLimitLeft = true;
			}
		} else if (this.gameObject.name == "StoveLimitRight") {
			if (other.gameObject.name == "Spatula") {
				collStoveLimitRight = true;
			}
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if (this.gameObject.name == "Bowl") {
			if (other.gameObject.name == "Egg" || other.gameObject.tag == "Egg") {
				collEgg = false;
			}
		} else if (this.gameObject.name == "BowlTriggerLeft") {
			if (other.gameObject.name == "Whisk") {
				collWhisk = false;
			}
		} else if (this.gameObject.name == "BowlTriggerRight") {
			if (other.gameObject.name == "Whisk") {
				collWhisk = false;
			}
		} else if (this.gameObject.name == "BowlLimitLeft") {
			if (other.gameObject.name == "Whisk") {
				collBowlLimitLeft = false;
				Debug.Log ("LEFT LIMIT LEAVE");
			}
		} else if (this.gameObject.name == "BowlLimitRight") {
			if (other.gameObject.name == "Whisk") {
				collBowlLimitRight = false;
				Debug.Log ("RIGHT LIMIT LEAVE");
			}
		} else if (this.gameObject.name == "StoveTriggerLeft") {
			if (other.gameObject.name == "Spatula") {
				collSpatula = false;
			}
		} else if (this.gameObject.name == "StoveTriggerRight") {
			if (other.gameObject.name == "Spatula") {
				collSpatula = false;
			}
		} else if (this.gameObject.name == "StoveLimitLeft") {
			if (other.gameObject.name == "Spatula") {
				collStoveLimitLeft = false;
			}
		} else if (this.gameObject.name == "StoveLimitRight") {
			if (other.gameObject.name == "Spatula") {
				collStoveLimitRight = false;
			}
		}
	}
		
	public bool getCollEgg()
	{
		return collEgg;
	}

	public bool getCollWhisk()
	{
		return collWhisk;
	}

	public bool getCollBowlLimitLLeft()
	{
		return collBowlLimitLeft;
	}

	public bool getCollBowlLimitLRight()
	{
		return collBowlLimitRight;
	}

	public void setCollWhisk(bool pVal)
	{
		collWhisk = pVal;
	}

	public void setBowlLimits(bool pVal)
	{
		collBowlLimitLeft = pVal;
		collBowlLimitRight = pVal;
	}

	public bool getCollSpatula()
	{
		return collSpatula;
	}

	public bool getCollStoveLimitLeft()
	{
		return collStoveLimitLeft;
	}

	public bool getCollStoveLimitRight()
	{
		return collStoveLimitRight;
	}

	public void setCollSpatula(bool pVal)
	{
		this.collSpatula = pVal;
	}

	public void setCollStoveLimits(bool pVal)
	{
		this.collStoveLimitLeft = pVal;
		this.collStoveLimitRight = pVal;
	}
}
