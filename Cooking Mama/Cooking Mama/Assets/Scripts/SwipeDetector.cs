using UnityEngine;
using System;

public enum State
{
    SwipeNotStarted,
    SwipeStarted
}

public enum InputDirection
{
	Left, Right, Top, Bottom, Circle, DiagUpRight, DiagUpLeft, DiagDownRight, DiagDownLeft, None
}

public class SwipeDetector : MonoBehaviour 
{
	private State state = State.SwipeNotStarted;
    private Vector2 startPoint;
    private DateTime timeSwipeStarted;
	private TimeSpan maxSwipeDuration = TimeSpan.FromSeconds(10);
	private TimeSpan minSwipeDuration = TimeSpan.FromMilliseconds(1);

	private TouchEntity entity;

	void Start()
	{
		entity = GetComponent<TouchEntity> ();
	}

	public TouchEntity DetectInputDirection()
	{

		if (state == State.SwipeNotStarted)
		{
			if (Input.GetMouseButtonDown(0))
			{
				timeSwipeStarted = DateTime.Now;
				state = State.SwipeStarted;
				startPoint = Input.mousePosition;

				//Debug.Log ("Start X: " + startPoint.x);
				//Debug.Log ("Start Y: " + startPoint.y);

				entity.setSwipePointX (startPoint.x);
				entity.setSwipePointY (startPoint.y);
			}
		}
		else if (state == State.SwipeStarted)
		{
			if (Input.GetMouseButtonUp(0))
			{
				TimeSpan timeDifference = DateTime.Now - timeSwipeStarted;
				if (timeDifference <= maxSwipeDuration && timeDifference >= minSwipeDuration)
				{
					Vector2 mousePosition = Input.mousePosition;
					Vector2 differenceVector = mousePosition - startPoint;
					float angle = Vector2.Angle(differenceVector, Vector2.right);
					Vector3 cross = Vector3.Cross(differenceVector, Vector2.right);

					if (cross.z > 0)
						angle = 360 - angle;

					state = State.SwipeNotStarted;

					if ((angle >= 337.5 && angle <= 360) || (angle >= 0 && angle <= 22.5)) {
						entity.setSwipeDirection(InputDirection.Right);
						return entity;
					} else if (angle >= 22.5 && angle <= 67.5) {
						entity.setSwipeDirection(InputDirection.DiagUpRight);
						return entity;
					} else if (angle >= 67.5 && angle <= 112.5) {
						entity.setSwipeDirection(InputDirection.Top);
						return entity;
					} else if (angle >= 112.5 && angle <= 157.5) {
						entity.setSwipeDirection(InputDirection.DiagUpLeft);
						return entity;
					} else if (angle >= 157.5 && angle <= 202.5) {
						entity.setSwipeDirection(InputDirection.Left);
						return entity;
					} else if (angle >= 202.5 && angle <= 247.5) {
						entity.setSwipeDirection(InputDirection.DiagDownLeft);
						return entity;
					} else if (angle > 247.5 && angle <= 292.5) {
						entity.setSwipeDirection(InputDirection.Bottom);
						return entity;
					} else if (angle > 292.5 && angle <= 337.5) {
						entity.setSwipeDirection(InputDirection.DiagDownRight);
						return entity;
					}

				}
			}
		}

		entity.setSwipeDirection (InputDirection.None);
		entity.setSwipePointX (-1);
		entity.setSwipePointY (-1);

		return entity;
    }

}
