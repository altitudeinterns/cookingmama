﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class ObjectRotator : MonoBehaviour, IDragHandler {

	private float sensitivity;
	private Vector3 mouseReference;
	private Vector3 mouseOffset;
	private Vector3 rotation;
	private bool isRotating;
	private bool isDone = false;
	private float objAngle = 0;

	private float ogRotAng = 0;
	private float ogTouchAng;
	private float rotationAngle;

	void Start ()
	{
		sensitivity = 0.4f;
		rotation = Vector3.zero;
	}

	public void OnDrag(PointerEventData eventData)
	{
		if(isRotating)
		{
			var spinnerScreenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
			ogTouchAng = (Mathf.Atan2(Input.mousePosition.y, Input.mousePosition.x) * Mathf.Rad2Deg);

			Vector3 position = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z); 

			spinnerScreenPoint = (position - spinnerScreenPoint);

			float newRot = Mathf.Atan2(spinnerScreenPoint.y, spinnerScreenPoint.x) * Mathf.Rad2Deg;
 			rotationAngle = ogRotAng + newRot - ogTouchAng;
			//ogRotAng = rotationAngle;
		}
	}
		
	void Update()
	{
		if (objAngle != 0) {

			//Debug.Log ("Rot Angle is: " + rotationAngle);
			//Debug.Log ("Obj Angle is: " + objAngle);

			if (rotationAngle <= (objAngle + 10f) && rotationAngle >= (objAngle - 10f))
				isDone = true;
			else if (rotationAngle != objAngle)
				gameObject.transform.rotation = Quaternion.AngleAxis (rotationAngle, Vector3.forward);

			//Debug.Log ("IsDone is " + isDone);
		}
	}

	void OnMouseDown()
	{
		// rotating flag
		if(objAngle != 0 || isDone != true)
			isRotating = true;

		//Debug.Log ("MOUSEDOWN isROT: " + isRotating);
		// store mouse
		//mouseReference = Input.mousePosition;

		ogRotAng = this.transform.eulerAngles.z;
		//Debug.Log ("OGROTANG: " + ogRotAng);
	}

	void OnMouseUp()
	{
		// rotating flag
		isRotating = false;
		//Debug.Log ("MOUSEUP isROT: " + isRotating);
	}

	public void SetObjAngle(float pAngle)
	{
		this.objAngle = pAngle;
	}

	public bool GetIsDone()
	{
		return isDone;
	}
}
