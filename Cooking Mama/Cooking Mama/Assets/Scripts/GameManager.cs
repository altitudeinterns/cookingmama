﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum GameState
{
	Start, RecipeStart, RecipeWait, RecipeFinish
}

public class GameManager : MonoBehaviour {

	private StartScript startScriptTest;
	private Draggable draggable;
	private SwipeDetector swipeDetector;
	private TouchEntity entity;
	private Collider2D coll2d;
	public Camera camera;

	public GameObject currOrderPanel;
	public Text currOrderPanelText;

	public GameObject currInstructionPanel;
	public Text currInstructionPanelText;

	public GameObject directionPanel;
	public Text directionText;

	//Game Variables
	private InputDirection? swipeDirectionNeeded;
	private GameState? gameState = GameState.Start;
	private string actionNeeded;
	private string dragTo;
	private string rotObj;
	private float spawnX;
	private float spawnY;
	private float timeWait = 3f;
	private bool timeFlag = true;

	//Recipe Variables
	private bool initRecipe = false;
	private string currRecipe;
	private int currStep;
	private int maxStep;
	private int numWhisk = 15;
	private int currWhisk = 0;
	private int numSpatula = 4;
	private int currSpatula = 0;

	//Swipe Variables
	private float swipeNeedLocX = 0.0f;
	private float swipeNeedLocY = 0.0f;
	private Vector3 needLoc;

	//Circle Variables
	private Vector2 initialFingerLocation;
	private Vector2 currentFingerLocation;
	private Vector2 middlePoint;
	private float currentAngle;
	private float prevAngle;
	private float turns = 0;

	//SpawnPoints
	public GameObject trBowlLeft;
	public GameObject trBowlRight;
	public GameObject trStoveLeft;
	public GameObject trStoveRight;
	public GameObject bowlLimitLeft;
	public GameObject bowlLimitRight;
	public GameObject stoveLimitLeft;
	public GameObject stoveLimitRight;
	public GameObject stoveLimitBottom;
	//public Transform toolSpawn;

	//Sprites
	private SpriteRenderer rendEgg;
	private SpriteRenderer rendSalt;
	private SpriteRenderer rendPepper;
	private SpriteRenderer rendMilk;
	private SpriteRenderer rendButter;

	private SpriteRenderer rendBowl;
	private SpriteRenderer rendPan;
	private SpriteRenderer rendStove;
	private SpriteRenderer rendBowlFront;
	private SpriteRenderer rendBowlBack;
	private SpriteRenderer rendWhisk;
	private SpriteRenderer rendSpatula;
	private SpriteRenderer rendPlate;

	private SpriteRenderer rendArrowLeft;
	private SpriteRenderer rendArrowRight;

	//Tools
	private GameObject goBowl;
	private GameObject goChoppingBoard;
	private GameObject goKnife;
	private GameObject goBowlFront;
	private GameObject goBowlBack;
	private GameObject goWhisk;
	private GameObject goStove;
	private GameObject goSpatula;
	private GameObject goPan;
	private GameObject goPlate;

	public Sprite sBowl;
	public Sprite sChoppingBoard;
	public Sprite sKnife;
	public Sprite sBowlFront;
	public Sprite sBowlBack;
	public Sprite sWhisk;
	public Sprite sStove;
	public Sprite sSpatula;
	public Sprite sPan;
	public Sprite sPlate;

	//UI
	private GameObject goArrowLeft;
	private GameObject goArrowRight;

	public Sprite sArrowLeft;
	public Sprite sArrowRight;

	//Scrambled Egg
	private GameObject goEgg;
	private GameObject goEggOpen;
	private GameObject goSalt;
	private GameObject goPepper;
	private GameObject goMilk;
	private GameObject goButter;

	public Sprite sEgg;
	public Sprite sEggOpen;
	public Sprite sSalt;
	public Sprite sPepper;
	public Sprite sMilk;
	public Sprite sButter;

	// Use this for initialization
	void Start () {

		swipeDetector = GetComponent<SwipeDetector> ();
		entity = GetComponent<TouchEntity> ();

		GameObject startGameObject = new GameObject ();
		startGameObject = GameObject.Find ("PlayerStart");
		startScriptTest = (StartScript) startGameObject.GetComponent ("StartScript");

		currOrderPanelText.text = currOrderPanelText.text + startScriptTest.GetRecipe ();
		currRecipe = startScriptTest.GetRecipe ();

		goBowl = new GameObject("Bowl");
		goChoppingBoard = new GameObject("ChoppingBoard");
		goKnife = new GameObject("Knife");
		goBowlFront = new GameObject ("BowlFront");
		goBowlBack = new GameObject ("BowlBack"); 
		goWhisk = new GameObject ("Whisk");
		goStove = new GameObject ("Stove");
		goSpatula = new GameObject ("Spatula");
		goPan = new GameObject ("Pan");
		goPlate = new GameObject ("Plate");

		goArrowLeft = new GameObject ("ArrowLeft");
		goArrowRight = new GameObject ("ArrowRight");

		goEgg = new GameObject("Egg");
		goEggOpen = new GameObject("EggOpen");
		goSalt = new GameObject ("Salt");
		goPepper = new GameObject ("Pepper");
		goMilk = new GameObject ("Milk");
		goButter = new GameObject ("Butter"); 

		trBowlRight = new GameObject ("BowlTriggerRight");
		trBowlLeft = new GameObject ("BowlTriggerLeft");

		trStoveLeft = new GameObject ("StoveTriggerLeft");
		trStoveRight = new GameObject ("StoveTriggerRight");

		bowlLimitLeft = new GameObject ("BowlLimitLeft");
		bowlLimitRight = new GameObject ("BowlLimitRight");

		stoveLimitLeft = new GameObject ("StoveLimitLeft");
		stoveLimitRight = new GameObject ("StoveLimitRight");
		stoveLimitBottom = new GameObject ("StoveLimitBottom");

		Vector3 midVec3 = new Vector3 (Screen.currentResolution.width / 2, Screen.currentResolution.height / 2);

		middlePoint = new Vector2 (camera.ScreenToWorldPoint (midVec3).x, camera.ScreenToWorldPoint (midVec3).y);

	}
	
	// Update is called once per frame
	void Update () {

		if (!timeFlag)
			timeWait -= Time.deltaTime;

		if (!timeFlag && timeWait <= 0) {
			rendSpatula.enabled = true;
			spawnX = -3f;
			spawnY = 1.5f;
			rendSpatula.transform.position = new Vector2 (spawnX, spawnY);
			goSpatula.GetComponent<ObjectFade> ().StartFadeIn ();
			goSpatula.AddComponent<Draggable> ();
			goSpatula.GetComponent<Draggable> ().setDragLeftEnabled (true);
			goSpatula.GetComponent<Draggable> ().setDragRightEnabled (true);
			timeFlag = true;
		}

		if (currStep == 5) {

			if(!(bowlLimitLeft.GetComponent<Collision> ().getCollBowlLimitLLeft ())){

				goWhisk.GetComponent<Draggable> ().setDragLeftEnabled (true);
			} 

			if(!(bowlLimitLeft.GetComponent<Collision> ().getCollBowlLimitLRight ())){

				goWhisk.GetComponent<Draggable> ().setDragRightEnabled (true);
			} 

			if (bowlLimitLeft.GetComponent<Collision> ().getCollBowlLimitLLeft ()) {

				goWhisk.GetComponent<Draggable> ().setDragLeftEnabled (false);
			} 

			if(bowlLimitRight.GetComponent<Collision> ().getCollBowlLimitLRight ()){

				goWhisk.GetComponent<Draggable> ().setDragRightEnabled (false);
			} 
		}

		if (currStep == 7) {

			Debug.Log ("UPDATE7777777");

			if(!(stoveLimitLeft.GetComponent<Collision> ().getCollStoveLimitLeft ())){

				goSpatula.GetComponent<Draggable> ().setDragLeftEnabled (true);
			} 

			if(!(stoveLimitLeft.GetComponent<Collision> ().getCollStoveLimitRight ())){

				goSpatula.GetComponent<Draggable> ().setDragRightEnabled (true);
			} 

			if (stoveLimitLeft.GetComponent<Collision> ().getCollStoveLimitLeft ()) {

				goSpatula.GetComponent<Draggable> ().setDragLeftEnabled (false);
			} 

			if(stoveLimitRight.GetComponent<Collision> ().getCollStoveLimitRight ()){

				goSpatula.GetComponent<Draggable> ().setDragRightEnabled (false);
			} 
		}

		var mousePos = Input.mousePosition;
		mousePos.z = -10;

		needLoc = camera.ScreenToWorldPoint (mousePos);

		if (gameState == GameState.Start) {

			if (currRecipe == "Scrambled Egg") {
				recipeEggStart ();
				gameState = GameState.RecipeWait;
			}
		} else if (gameState == GameState.RecipeWait) {

			if (actionNeeded == "swipe") {

				entity = swipeDetector.DetectInputDirection ();

				if (verifySwipe (entity, needLoc)) {

					if ((currStep + 1) == maxStep) {
						gameState = GameState.RecipeFinish;
					} else if ((currStep < maxStep) && ((currStep + 1) < maxStep)) {
						currStep++;
						recipeEgg ();
					}
				}

			} else if (actionNeeded == "drag") {

				if (verifyDrag ()) {

					if ((currStep + 1) == maxStep) {
						gameState = GameState.RecipeFinish;
					} else if ((currStep < maxStep) && ((currStep + 1) < maxStep)) {

						if (timeFlag) {
							
							currStep++;
							recipeEgg ();
						}
					}
				}

			} else if (actionNeeded == "rotate") {

				if (verifyRotate ()) {

					if ((currStep + 1) == maxStep) {
						gameState = GameState.RecipeFinish;
					} else if ((currStep < maxStep) && ((currStep + 1) < maxStep)) {
						currStep++;
						recipeEgg ();
					}
				}

			}
		}

	}

	public bool verifySwipe(TouchEntity pEntity, Vector3 needLoc)
	{
		if ((pEntity.getSwipeDirection() == swipeDirectionNeeded) &&
			(needLoc.x > swipeNeedLocX - 2) &&
			(needLoc.x < swipeNeedLocX + 2) &&
			(needLoc.y > swipeNeedLocY - 2) &&
			(needLoc.y < swipeNeedLocY + 2))
			return true;

		return false;
	}

	public bool verifyDrag()
	{
		
		if (dragTo == "bowl") {
			if (goBowl.GetComponent<Collision> ().getCollEgg ())
				return true;
		} else if (dragTo == "bowlSide") {

			if (!(bowlLimitLeft.GetComponent<Collision> ().getCollBowlLimitLLeft ()) && !(bowlLimitLeft.GetComponent<Collision> ().getCollBowlLimitLRight ())) {
				
				if (currWhisk < numWhisk) {
					if (trBowlLeft.GetComponent<Collision> ().getCollWhisk () || trBowlRight.GetComponent<Collision> ().getCollWhisk ()) {
						currWhisk++;
						trBowlLeft.GetComponent<Collision> ().setCollWhisk (false);
						trBowlRight.GetComponent<Collision> ().setCollWhisk (false);
					}
				} else if (currWhisk == numWhisk) {
					return true;
				}
			}
		} else if (dragTo == "stoveSide") {

			if (!(stoveLimitLeft.GetComponent<Collision> ().getCollStoveLimitLeft ()) && !(stoveLimitLeft.GetComponent<Collision> ().getCollStoveLimitRight ())) {

				if (currSpatula < numSpatula) {
					if (trStoveLeft.GetComponent<Collision> ().getCollSpatula () || trStoveRight.GetComponent<Collision> ().getCollSpatula ()) {
						currSpatula++;
						trStoveLeft.GetComponent<Collision> ().setCollSpatula (false);
						trStoveRight.GetComponent<Collision> ().setCollSpatula (false);
					}
				} else if (currSpatula == numSpatula) {
					return true;
				}
			}
		}

		return false;
	}

	public bool verifyRotate()
	{
		if (rotObj == "bowl") {
			if (goBowl.GetComponent<ObjectRotator> ().GetIsDone ())
				return true;
		}

		return false;
	}

	// RECIPES

	public void recipeEggStart()
	{
		currStep = 0; 
		maxStep = 20;

		initRecipe = false;
		recipeEgg ();
	}

	public void recipeEgg()
	{
		switch (currStep) {
		case 0: // Initialize stuff for recipes

			actionNeeded = "drag";
			dragTo = "bowl";

			Debug.Log ("Case 0");

			if (initRecipe == false) {

				spawnX = 5f;
				spawnY = 0f;
				rendEgg = goEgg.AddComponent<SpriteRenderer> ();
				rendEgg.sprite = sEgg;
				rendEgg.transform.position = new Vector2 (spawnX, spawnY);
				rendEgg.sortingLayerName = "Food";

				goEgg.AddComponent<Draggable>();
				goEgg.GetComponent<Draggable> ().setDragLeftEnabled (true);
				goEgg.GetComponent<Draggable> ().setDragRightEnabled (true);
				goEgg.AddComponent<Rigidbody2D> ();
				goEgg.AddComponent<BoxCollider2D>();
				goEgg.GetComponent<BoxCollider2D> ().size = new Vector2 (2f, 3f);
				goEgg.GetComponent<BoxCollider2D> ().offset = new Vector2 (-0.5f, 0f);
				goEgg.AddComponent<ObjectFade> ();
				//goEgg.GetComponent<ObjectFade> ().StartFadeIn();

				spawnX = -0.3f;
				spawnY = -1.8f;
				rendBowl = goBowl.AddComponent<SpriteRenderer> ();
				rendBowl.sprite = sBowl;
				rendBowl.transform.position = new Vector2 (spawnX, spawnY);
				rendBowl.sortingLayerName = "ToolFront";

				goBowl.AddComponent<ObjectFade> ();
				goBowl.AddComponent<Collision> ();
				goBowl.AddComponent<Rigidbody2D> ();
				goBowl.GetComponent<Rigidbody2D> ().isKinematic = true;
				goBowl.AddComponent<BoxCollider2D> ();
				goBowl.GetComponent<BoxCollider2D> ().size = new Vector2 (3f, 2.5f);
				goBowl.GetComponent<BoxCollider2D> ().offset = new Vector2 (-0.5f, 0f);
				goBowl.GetComponent<BoxCollider2D> ().isTrigger = true;

				swipeNeedLocX = rendEgg.transform.position.x;
				swipeNeedLocY = rendEgg.transform.position.y;

				//Debug.Log ("Swipe Need X: " + swipeNeedLocX);
				//Debug.Log ("Swipe Need Y: " + swipeNeedLocY);

				currInstructionPanelText.text = "Drag the egg to the bowl to crack it";

				//Debug.Log ("Rend X: " + rendEgg.transform.position.x);
				//Debug.Log ("Rend Y: " + rendEgg.transform.position.y);

				initRecipe = true;
			}
				
			break;
		case 1: 

			actionNeeded = "swipe";

			rendEgg.sprite = sEggOpen;

			Destroy (goEgg.GetComponent<Draggable> ());

			swipeNeedLocX = rendEgg.transform.position.x;
			swipeNeedLocY = rendEgg.transform.position.y;

			currInstructionPanelText.text = "Swipe diag down left on the egg to pour content in bowl";
			swipeDirectionNeeded = InputDirection.DiagDownLeft;

			break;
		case 2: 

			actionNeeded = "swipe";
			swipeDirectionNeeded = InputDirection.Bottom;
			currInstructionPanelText.text = "Swipe down to add salt";

			//rendEgg.enabled = false;
			goEgg.GetComponent<ObjectFade> ().StartFadeOut ();

			spawnX = 2f;
			spawnY = 1f;
			rendSalt = goSalt.AddComponent<SpriteRenderer> ();
			rendSalt.sprite = sSalt;
			rendSalt.transform.position = new Vector2 (spawnX, spawnY);
			rendSalt.sortingLayerName = "Food";

			goSalt.AddComponent<ObjectFade> ();
			goSalt.GetComponent<ObjectFade> ().StartFadeIn();

			swipeNeedLocX = rendSalt.transform.position.x;
			swipeNeedLocY = rendSalt.transform.position.y;
			goSalt.GetComponent<ObjectFade> ().StartFadeIn();

			break;
		case 3:

			actionNeeded = "swipe";
			swipeDirectionNeeded = InputDirection.Bottom;
			currInstructionPanelText.text = "Swipe down to add pepper";

			goSalt.GetComponent<ObjectFade> ().StartFadeOut ();
			//rendSalt.enabled = false;

			spawnX = 2f;
			spawnY = 2f;
			rendPepper = goPepper.AddComponent<SpriteRenderer> ();
			rendPepper.sprite = sPepper;
			rendPepper.transform.position = new Vector2 (spawnX, spawnY);
			rendPepper.sortingLayerName = "Food";

			goPepper.AddComponent<ObjectFade> ();
			goPepper.GetComponent<ObjectFade> ().StartFadeIn ();

			swipeNeedLocX = rendPepper.transform.position.x;
			swipeNeedLocY = rendPepper.transform.position.y;

			break;
		case 4:

			actionNeeded = "swipe";
			swipeDirectionNeeded = InputDirection.Bottom;
			currInstructionPanelText.text = "Swipe down to pour milk";

			//rendPepper.enabled = false;
			goPepper.GetComponent<ObjectFade> ().StartFadeOut ();

			spawnX = 0.5f;
			spawnY = 1f;
			rendMilk = goMilk.AddComponent<SpriteRenderer> ();
			rendMilk.sprite = sMilk;
			rendMilk.transform.localScale = new Vector3 (2f, 2f, 1f);
			rendMilk.transform.position = new Vector2 (spawnX, spawnY);
			rendMilk.sortingLayerName = "Food";

			goMilk.AddComponent<ObjectFade> ();
			goMilk.GetComponent<ObjectFade> ().StartFadeIn ();

			swipeNeedLocX = rendMilk.transform.position.x;
			swipeNeedLocY = rendMilk.transform.position.y;

			break;
		case 5:

			goMilk.GetComponent<ObjectFade> ().StartFadeOut ();
			rendBowl.enabled = false;

			actionNeeded = "drag";
			dragTo = "bowlSide";
			currInstructionPanelText.text = "Drag the whisk to beat mixture";

			spawnX = -0.3f;
			spawnY = -1.8f;
			rendBowlFront = goBowlFront.AddComponent<SpriteRenderer> ();
			rendBowlFront.sprite = sBowlFront;
			rendBowlFront.transform.position = new Vector2 (spawnX, spawnY);
			rendBowlFront.sortingLayerName = "ToolFront";
			goBowlFront.AddComponent<ObjectFade> ();

			spawnX = -0.3f;
			spawnY = -1.8f;
			rendBowlBack = goBowlBack.AddComponent<SpriteRenderer> ();
			rendBowlBack.sprite = sBowlBack;
			rendBowlBack.transform.position = new Vector2 (spawnX, spawnY);
			rendBowlBack.sortingLayerName = "ToolBack";
			goBowlBack.AddComponent<ObjectFade> ();

			spawnX = -2f;
			spawnY = -1f;
			trBowlLeft.transform.position = new Vector2 (spawnX, spawnY);
			trBowlLeft.AddComponent<Collision> ();
			trBowlLeft.AddComponent<Rigidbody2D> ();
			trBowlLeft.GetComponent<Rigidbody2D> ().isKinematic = true;
			trBowlLeft.AddComponent<BoxCollider2D> ();
			trBowlLeft.GetComponent<BoxCollider2D> ().size = new Vector2 (0.5f, 0.5f);
			trBowlLeft.GetComponent<BoxCollider2D> ().offset = new Vector2 (0f, 0f);
			trBowlLeft.GetComponent<BoxCollider2D> ().isTrigger = true;

			spawnX = 0.7f;
			spawnY = -1f;
			trBowlRight.transform.position = new Vector2 (spawnX, spawnY);
			trBowlRight.AddComponent<Collision> ();
			trBowlRight.AddComponent<Rigidbody2D> ();
			trBowlRight.GetComponent<Rigidbody2D> ().isKinematic = true;
			trBowlRight.AddComponent<BoxCollider2D> ();
			trBowlRight.GetComponent<BoxCollider2D> ().size = new Vector2 (0.5f, 0.5f);
			trBowlRight.GetComponent<BoxCollider2D> ().offset = new Vector2 (0f, 0f);
			trBowlRight.GetComponent<BoxCollider2D> ().isTrigger = true;

			spawnX = -0.3f;
			spawnY = 0f;
			rendWhisk = goWhisk.AddComponent<SpriteRenderer> ();
			rendWhisk.sprite = sWhisk;
			rendWhisk.transform.localScale = new Vector3 (2f, 2f, 1f);
			rendWhisk.transform.position = new Vector2 (spawnX, spawnY);
			rendWhisk.sortingLayerName = "ToolMid";
			goWhisk.AddComponent<Draggable> ();
			goWhisk.GetComponent<Draggable> ().setDragLeftEnabled (true);
			goWhisk.GetComponent<Draggable> ().setDragRightEnabled (true);
			goWhisk.AddComponent<Rigidbody2D> ();
			goWhisk.AddComponent<BoxCollider2D>();
			goWhisk.GetComponent<BoxCollider2D> ().size = new Vector2 (1.05f, 1.5f);
			goWhisk.GetComponent<BoxCollider2D> ().offset = new Vector2 (-0.5f, 0.5f);
			goWhisk.AddComponent<ObjectFade> ();
		
			spawnX = -3.25f;
			spawnY = -2.5f;
			bowlLimitLeft.transform.position = new Vector2 (spawnX, spawnY);
			bowlLimitLeft.AddComponent<Collision> ();
			bowlLimitLeft.AddComponent<Rigidbody2D> ();
			bowlLimitLeft.GetComponent<Rigidbody2D> ().isKinematic = true;
			bowlLimitLeft.AddComponent<BoxCollider2D> ();
			bowlLimitLeft.GetComponent<BoxCollider2D> ().size = new Vector2 (0.5f, 0.5f);
			bowlLimitLeft.GetComponent<BoxCollider2D> ().offset = new Vector2 (0f, 0f);
			bowlLimitLeft.GetComponent<BoxCollider2D> ().isTrigger = true;

			spawnX = 1.25f;
			spawnY = -2.5f;
			bowlLimitRight.transform.position = new Vector2 (spawnX, spawnY);
			bowlLimitRight.AddComponent<Collision> ();
			bowlLimitRight.AddComponent<Rigidbody2D> ();
			bowlLimitRight.GetComponent<Rigidbody2D> ().isKinematic = true;
			bowlLimitRight.AddComponent<BoxCollider2D> ();
			bowlLimitRight.GetComponent<BoxCollider2D> ().size = new Vector2 (0.5f, 0.5f);
			bowlLimitRight.GetComponent<BoxCollider2D> ().offset = new Vector2 (0f, 0f);
			bowlLimitRight.GetComponent<BoxCollider2D> ().isTrigger = true;

			break;
		case 6:

			trBowlLeft.SetActive (false);
			trBowlRight.SetActive (false);
		
			goBowlFront.GetComponent<ObjectFade> ().StartFadeOut ();
			goBowlBack.GetComponent<ObjectFade> ().StartFadeOut ();
			goWhisk.GetComponent<ObjectFade> ().StartFadeOut ();

			actionNeeded = "swipe";
			swipeDirectionNeeded = InputDirection.Bottom;
			currInstructionPanelText.text = "Swipe down to slice butter";

			spawnX = -0.5f;
			spawnY = 0.7f;
			rendButter = goButter.AddComponent<SpriteRenderer> ();
			rendButter.sprite = sButter;
			rendButter.transform.localScale = new Vector3 (2f, 2f, 1f);
			rendButter.transform.position = new Vector2 (spawnX, spawnY);
			rendButter.sortingLayerName = "Food";

			swipeNeedLocX = rendButter.transform.position.x;
			swipeNeedLocY = rendButter.transform.position.y;

			spawnX = 0.5f;
			spawnY = 0.5f;
			rendStove = goStove.AddComponent<SpriteRenderer> ();
			rendStove.sprite = sStove;
			rendStove.transform.localScale = new Vector3 (4f, 4f, 1f);
			rendStove.transform.position = new Vector2 (spawnX, spawnY);
			rendStove.sortingLayerName = "ToolFront";
			goStove.AddComponent<ObjectFade> ();


			goSpatula.AddComponent<Draggable> ();
			goSpatula.GetComponent<Draggable> ().setDragLeftEnabled (true);
			goSpatula.GetComponent<Draggable> ().setDragRightEnabled (true);
			goSpatula.AddComponent<Rigidbody2D> ();
			goSpatula.AddComponent<BoxCollider2D>();
			goSpatula.GetComponent<BoxCollider2D> ().size = new Vector2 (0.3f, 0.3f);
			goSpatula.GetComponent<BoxCollider2D> ().offset = new Vector2 (0.25f, -0.5f);

			break;
		case 7:

			Debug.Log ("CASE7777777");

			rendButter.enabled = false;

			actionNeeded = "drag";
			dragTo = "stoveSide";
			currInstructionPanelText.text = "Drag the spatula to spread the butter";

			spawnX = -3f;
			spawnY = 1.5f;
			rendSpatula = goSpatula.AddComponent<SpriteRenderer> ();
			rendSpatula.sprite = sSpatula;
			rendSpatula.transform.localScale = new Vector3 (2f, 2f, 1f);
			rendSpatula.transform.position = new Vector2 (spawnX, spawnY);
			rendSpatula.sortingLayerName = "ToolMid";
			goSpatula.AddComponent<ObjectFade> ();

			spawnX = -3.8f;
			spawnY = -0.5f;
			trStoveLeft.transform.position = new Vector2 (spawnX, spawnY);
			trStoveLeft.AddComponent<Collision> ();
			trStoveLeft.AddComponent<Rigidbody2D> ();
			trStoveLeft.GetComponent<Rigidbody2D> ().isKinematic = true;
			trStoveLeft.AddComponent<BoxCollider2D> ();
			trStoveLeft.GetComponent<BoxCollider2D> ().size = new Vector2 (0.5f, 0.5f);
			trStoveLeft.GetComponent<BoxCollider2D> ().offset = new Vector2 (0f, 0f);
			trStoveLeft.GetComponent<BoxCollider2D> ().isTrigger = true;

			spawnX = -0.9f;
			spawnY = -0.5f;
			trStoveRight.transform.position = new Vector2 (spawnX, spawnY);
			trStoveRight.AddComponent<Collision> ();
			trStoveRight.AddComponent<Rigidbody2D> ();
			trStoveRight.GetComponent<Rigidbody2D> ().isKinematic = true;
			trStoveRight.AddComponent<BoxCollider2D> ();
			trStoveRight.GetComponent<BoxCollider2D> ().size = new Vector2 (0.5f, 0.5f);
			trStoveRight.GetComponent<BoxCollider2D> ().offset = new Vector2 (0f, 0f);
			trStoveRight.GetComponent<BoxCollider2D> ().isTrigger = true;

			spawnX = -4.3f;
			spawnY = -0.5f;
			stoveLimitLeft.transform.position = new Vector2 (spawnX, spawnY);
			stoveLimitLeft.AddComponent<Collision> ();
			stoveLimitLeft.AddComponent<Rigidbody2D> ();
			stoveLimitLeft.GetComponent<Rigidbody2D> ().isKinematic = true;
			stoveLimitLeft.AddComponent<BoxCollider2D> ();
			stoveLimitLeft.GetComponent<BoxCollider2D> ().size = new Vector2 (0.5f, 0.5f);
			stoveLimitLeft.GetComponent<BoxCollider2D> ().offset = new Vector2 (0f, 0f);
			stoveLimitLeft.GetComponent<BoxCollider2D> ().isTrigger = true;

			spawnX = -0.4f;
			spawnY = -0.5f;
			stoveLimitRight.transform.position = new Vector2 (spawnX, spawnY);
			stoveLimitRight.AddComponent<Collision> ();
			stoveLimitRight.AddComponent<Rigidbody2D> ();
			stoveLimitRight.GetComponent<Rigidbody2D> ().isKinematic = true;
			stoveLimitRight.AddComponent<BoxCollider2D> ();
			stoveLimitRight.GetComponent<BoxCollider2D> ().size = new Vector2 (0.5f, 0.5f);
			stoveLimitRight.GetComponent<BoxCollider2D> ().offset = new Vector2 (0f, 0f);
			stoveLimitRight.GetComponent<BoxCollider2D> ().isTrigger = true;

			spawnX = -2.5f;
			spawnY = -1.1f;
			stoveLimitBottom.transform.position = new Vector2 (spawnX, spawnY);
			stoveLimitBottom.AddComponent<Collision> ();
			stoveLimitBottom.AddComponent<Rigidbody2D> ();
			stoveLimitBottom.GetComponent<Rigidbody2D> ().isKinematic = true;
			stoveLimitBottom.AddComponent<BoxCollider2D> ();
			stoveLimitBottom.GetComponent<BoxCollider2D> ().size = new Vector2 (0.5f, 0.5f);
			stoveLimitBottom.GetComponent<BoxCollider2D> ().offset = new Vector2 (0f, 0f);
			stoveLimitBottom.GetComponent<BoxCollider2D> ().isTrigger = true;

			break;
		case 8: 

			rendSpatula.enabled = false;
			currInstructionPanelText.text = "Drag to rotate the bowl to pour egg mixture";

			actionNeeded = "rotate";
			rotObj = "bowl";

			spawnX = 0.5f;
			spawnY = 1f;
			rendBowl.transform.position = new Vector2 (spawnX, spawnY);
			rendBowl.enabled = true;
			goBowl.AddComponent<ObjectRotator> ();
			goBowl.GetComponent<ObjectRotator> ().SetObjAngle (60f);

			break;
		case 9:

			goSpatula.GetComponent<ObjectFade> ().StartFadeOut ();
			Destroy(goSpatula.GetComponent<Draggable>());
			timeWait = 3f;
			timeFlag = false;
			rendBowl.enabled = false;
			rendSpatula.enabled = true;

			actionNeeded = "drag";
			dragTo = "stoveSide";

			currInstructionPanelText.text = "Stir(?) the egg when prompted to cook the egg";
			directionText.text = "";

			currSpatula = 0;
			numSpatula = 2;

			break;
		case 10:
			
			goSpatula.GetComponent<ObjectFade> ().StartFadeOut ();
			Destroy(goSpatula.GetComponent<Draggable>());
			timeWait = 3f;
			timeFlag = false;

			currSpatula = 0;
			numSpatula = 2;
			directionText.text = "";

			break;
		case 11:

			goSpatula.GetComponent<ObjectFade> ().StartFadeOut ();
			Destroy(goSpatula.GetComponent<Draggable>());
			timeWait = 3f;
			timeFlag = false;

			currSpatula = 0;
			numSpatula = 2;
			directionText.text = "";

			break;
		case 12:

			goSpatula.GetComponent<ObjectFade> ().StartFadeOut ();
			Destroy (goSpatula.GetComponent<Draggable> ());
			timeWait = 3f;
			timeFlag = false;

			currSpatula = 0;
			numSpatula = 2;
			directionText.text = "";

			break;
		case 13: 

			goSpatula.GetComponent<ObjectFade> ().StartFadeOut ();
			goStove.GetComponent<ObjectFade> ().StartFadeOut ();
			directionText.text = "";

			spawnX = -3.05f;
			spawnY = -0.66f;
			rendPan = goPan.AddComponent<SpriteRenderer> ();
			rendPan.sprite = sPan;
			rendPan.transform.localScale = new Vector3 (4f, 4f, 1f);
			rendPan.transform.position = new Vector2 (spawnX, spawnY);
			rendPan.sortingLayerName = "ToolMid";
			goPan.AddComponent<Draggable> ();
			goPan.GetComponent<Draggable> ().setDragLeftEnabled (true);
			goPan.GetComponent<Draggable> ().setDragRightEnabled (true);
			goPan.AddComponent<Rigidbody2D> ();
			goPan.AddComponent<BoxCollider2D> ();
			goPan.GetComponent<BoxCollider2D> ().size = new Vector2 (0.3f, 0.3f);
			goPan.GetComponent<BoxCollider2D> ().offset = new Vector2 (0.25f, -0.5f);

			spawnX = 3f;
			spawnY = 0f;
			rendPlate = goPlate.AddComponent<SpriteRenderer> ();
			rendPlate.sprite = sPlate;
			rendPlate.transform.localScale = new Vector3 (1.5f, 1.5f, 1f);
			rendPlate.transform.position = new Vector2 (spawnX, spawnY);
			rendPlate.sortingLayerName = "ToolBack";

			break;
		case 14:



			break;
		}
	}

}