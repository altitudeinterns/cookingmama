﻿using UnityEngine;
using System.Collections;

public class TouchEntity : MonoBehaviour 
{
	private float swipePointX = 0;
	private float swipePointY = 0;
	private InputDirection? swipeDirection = InputDirection.None;

	public float getSwipePointX()
	{
		return this.swipePointX;
	}

	public float getSwipePointY()
	{
		return this.swipePointY;
	}

	public void setSwipePointX(float pSwipePointX)
	{
		this.swipePointX = pSwipePointX;
	}

	public void setSwipePointY(float pSwipePointY)
	{
		this.swipePointY = pSwipePointY;
	}

	public InputDirection? getSwipeDirection()
	{
		return this.swipeDirection;
	}

	public void setSwipeDirection(InputDirection? pSwipeDirection)
	{
		this.swipeDirection = pSwipeDirection;
	}
}

