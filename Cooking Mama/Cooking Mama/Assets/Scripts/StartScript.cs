﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartScript : MonoBehaviour {

	public GameObject startPanel;
	public GameObject startBackground;
	public string selectedRecipe;

	public Button recipeEggButton;

	void Awake()
	{
		DontDestroyOnLoad (this);
	}

	public void RecipeEggButtonClick()
	{
		Debug.Log ("Start");
		selectedRecipe = "Scrambled Egg";
		Debug.Log (selectedRecipe);
		SceneManager.LoadScene ("CookingMamaGame", LoadSceneMode.Single);
	}

	public string GetRecipe()
	{
		return selectedRecipe;
	}
}
