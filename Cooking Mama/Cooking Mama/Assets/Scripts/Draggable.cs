﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Draggable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

	private bool dragLeftEnabled = true;
	private bool dragRightEnabled = true;
	private bool isReset = true;

	void Update()
	{
		if (!isReset) {

			if (Input.GetMouseButtonUp(0)) {
				isReset = true;
				Debug.Log ("IS RESET");
			}

		}
	}

	public void OnBeginDrag(PointerEventData eventData) {
		Debug.Log ("OnBeginDrag");
	}

	public void OnDrag(PointerEventData eventData) {

		var loc = new Vector3 (eventData.position.x, eventData.position.y, 10);

		var locWorldPoint = Camera.main.ScreenToWorldPoint (loc);

		if (!dragLeftEnabled || !dragRightEnabled) {

			if(this.name == "Whisk"){

				if (!dragLeftEnabled) {
					Debug.Log ("HIT LEFT");
					this.transform.position = new Vector3 (-0.78f, -1.5f, 10f);

				} else if (!dragRightEnabled) {
					Debug.Log ("HIT RIGHT");
					this.transform.position = new Vector3 (0.78f, -1.5f, 10f);

				}

			} else if(this.name == "Spatula"){

				if (!dragLeftEnabled) {

					this.transform.position = new Vector3 (-2.5f, 1f, 10f);
				} else if (!dragRightEnabled) {

					this.transform.position = new Vector3 (-2.5f, 1f, 10f);
				}
			}
		} else if (dragLeftEnabled && dragRightEnabled && isReset == true) {
			this.transform.position = locWorldPoint;
		}

	}
	
	public void OnEndDrag(PointerEventData eventData) {

		Debug.Log ("OnEndDrag");
	}

	public void OnPointerUp(PointerEventData eventData) {

		isReset = true;

		Debug.Log ("RESET DRAG");
	}

	public void setDragLeftEnabled(bool pVal)
	{
		if (pVal == false)
			isReset = false;

		dragLeftEnabled = pVal;
	}

	public void setDragRightEnabled(bool pVal)
	{
		if (pVal == false)
			isReset = false;

		dragRightEnabled = pVal;
	}

	public bool getDragLeftEnabled()
	{
		return dragLeftEnabled;
	}

	public bool getDragRightEnabled()
	{
		return dragRightEnabled;
	}

}
